function getAllEvens (collection) {
  return collection.filter(function (item) { return item % 2 === 0; });
}

function getAllIncrementEvens (start, end) {
  const collection = [];
  for (let i = start; i <= end; i++) {
    collection.push(i);
  }
  return collection.filter(item => item % 2 === 0);
}

function getIntersectionOfcollections (collection1, collection2) {
  return collection1.filter(item1 => {
    for (const item2 of collection2) {
      if (item1 === item2) return true;
    }
    return false;
  });
}

function getUnionOfcollections (collection1, collection2) {
  const collection = collection1.concat(collection2);
  return collection.filter((x, index, self) => self.indexOf(x) === index);
}

function countItems (collection) {
  const items = collection.filter((x, index, self) => self.indexOf(x) === index);
  const output = {};
  for (const item of items) {
    output[item] = 0;
  }
  for (const item of collection) {
    output[item]++;
  }
  return output;
}

export {
  getAllEvens,
  getAllIncrementEvens,
  getIntersectionOfcollections,
  getUnionOfcollections,
  countItems
};
