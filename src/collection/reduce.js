function getMaxNumber (collction) {
  return collction.reduce((pre, current) => Math.max(pre, current));
}

function isSameCollection (collction1, collction2) {
  if (collction1.length !== collction2.length) return false;

  for (const index in collction1) {
    if (collction1[index] !== collction2[index]) return false;
  }

  return true;
}

function sum (collction) {
  return collction.reduce((pre, current) => pre + current);
}

function computeAverage (collction) {
  return collction.reduce((pre, current, index) => (pre * index + current) / (index + 1));
}

function lastEven (collction) {
  // filter方法  return collction.filter(item => item % 2 === 0).pop();

  return collction.reduce((pre, current) => {
    if (current % 2 === 0 && current > pre) return current;
    else return pre;
  }, 0);
}

export { getMaxNumber, isSameCollection, computeAverage, sum, lastEven };
