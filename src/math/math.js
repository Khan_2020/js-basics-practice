function upwardNumber (num) {
  return Math.ceil(num);
}

function downwardNumber (num) {
  return Math.floor(num);
}

function roundNumber (num) {
  return Math.round(num);
}

function random (startNum, endNum) {
  return startNum + Math.random() * (endNum - startNum);
}

function minNumber (arr) {
  return Math.min.apply(null, arr);
}

function maxNumber (arr) {
  // 也可以这样 return Math.max.apply(null, arr);
  return Math.max(...arr);
}

export {
  upwardNumber,
  downwardNumber,
  roundNumber,
  random,
  maxNumber,
  minNumber
};
