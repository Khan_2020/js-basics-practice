function getDayOfMonth (dateStr) {
  const date = new Date(dateStr);
  return date.getDate();
}

function getDayOfWeek (dateStr) {
  const date = new Date(dateStr);
  return date.getDay();
}

function getYear (dateStr) {
  const date = new Date(dateStr);
  return date.getFullYear();
}

function getMonth (dateStr) {
  const date = new Date(dateStr);
  return date.getMonth();
}

function getMilliseconds (dateStr) {
  const date = new Date(dateStr);
  // const date2 = new Date('January 1, 1970 00:00:00');

  return date.getTime(); // - date2.getTime();
  // return 1563729300000;
}

export { getDayOfMonth, getDayOfWeek, getYear, getMonth, getMilliseconds };
