function joinArrays (arr1, arr2) {
  const output = arr1;
  for (const arr of arr2) {
    output.push(arr);
  }
  return output;
}

function checkAdult (arr, age) {
  for (const ageArr of arr) {
    if (ageArr > age) return false;
  }
  return true;
}

function findAdult (arr, age) {
  for (const ageArr of arr) {
    if (ageArr > age) return ageArr;
  }
  return -1;
}

function convertArrToStr (arr, str) {
  let finalString = '';
  for (const s of arr) {
    finalString += '-' + s;
  }
  return finalString.substring(1, finalString.length);
}

function removeLastEle (arr) {
  const newArr = arr;
  newArr.pop();
  return newArr;
}

function addNewItem (arr, item) {
  const newArr = arr;
  newArr.push(item);
  return newArr;
}

function removeFirstItem (arr) {
  const newArr = arr;
  newArr.shift();
  return newArr;
}

function addNewItemToBeginArr (arr, item) {
  const newArr = arr;
  newArr.unshift(item);
  return newArr;
}

function reverseOrder (arr) {
  let newArr = arr;
  newArr = newArr.reverse();
  return newArr;
}

function selectElements (arr, start, end) {
  const newArr = [];
  for (let i = start; i < end; i++) {
    newArr.push(arr[i]);
  }
  return newArr;
}

function addItemsToArray (arr, index, howmany, item) {
  arr.splice(index, howmany, item);
  return arr;
}

function sortASC (arr) { 
  return arr.sort(function (a, b) { return a - b; });
}

function sortDESC (arr) {
  return arr.sort(function (a, b) { return b - a; });
}

export {
  joinArrays,
  checkAdult,
  findAdult,
  convertArrToStr,
  removeLastEle,
  addNewItem,
  removeFirstItem,
  addNewItemToBeginArr,
  reverseOrder,
  selectElements,
  addItemsToArray,
  sortASC,
  sortDESC
};
