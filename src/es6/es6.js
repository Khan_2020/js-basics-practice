function flatArray (arr) {
  return arr.flat();
}

function aggregateArray (arr) {
  return arr.map(item => [item * 2]);
}

function getEnumerableProperties (obj) {
  const res = [];
  for (const property in obj) res.push(property);
  return res;
}

function removeDuplicateItems (arr) {
  return arr.filter((x, index, self) => self.indexOf(x) === index);
}

function removeDuplicateChar (str) {
  return str.split('').filter((x, index, self) =>
    self.indexOf(x) === index).reduce((pre, cur) => pre + cur, '');
}

function addItemToSet (set, item) {
  set.add(item);
  return set;
}

function removeItemToSet (set, item) {
  set.delete(item);
  return set;
}

function countItems (arr) {
  const items = arr.filter((x, index, self) => self.indexOf(x) === index);
  const output = new Map();
  for (const item of items) {
    output.set(item, 0);
  }
  for (const item of arr) {
    output.set(item, output.get(item) + 1);
  }
  return output;
}

export {
  flatArray,
  aggregateArray,
  getEnumerableProperties,
  removeDuplicateItems,
  removeDuplicateChar,
  addItemToSet,
  removeItemToSet,
  countItems
};
